#!/bin/bash
userhome=$(whoami)
#mkdir /$userhome/.config
mkdir -p /home/$userhome/.config

sleep 2
cd /home/$userhome/.config
sleep 2 
printf "Would you like to pull in all files needed for this rice?\n>"
read ans
case $ans in 
	yes)
		printf "Starting proccess\n"
		git clone https://gitlab.com/DownMeSkid/dwm.git
		git clone https://gitlab.com/DownMeSkid/st.git
		git clone https://gitlab.com/DownMeSkid/dmenu.git
		git clone https://gitlab.com/DownMeSkid/slstatus.git
		sleep 2.5
		printf "Done Downloading files...\n"
		cd dwm
		doas make clean install 
		cd ../st
        	doas make clean install
		cd ../dmenu
		doas make clean install
		cd ../slstatus 
		doas make clean install
		cd $userhome
		clear
		printf "Finished Installing Rice...\n"
		sleep 2
		exit
		;;
	no)
		printf "Exiting Script...\n"
		exit
		;;
esac

